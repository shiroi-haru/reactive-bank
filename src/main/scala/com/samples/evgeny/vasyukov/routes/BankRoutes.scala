package com.samples.evgeny.vasyukov.routes

import com.samples.evgeny.vasyukov.bank.Bank
import com.samples.evgeny.vasyukov.bank.BankMessages._

import akka.http.scaladsl.marshalling._
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.util.Timeout

import scala.util.control.NonFatal
import scala.util.{ Failure, Success }

trait BankRoutes {
  implicit val marshaller = Marshaller.strict[ResultBankMessage, HttpResponse] { result =>
    val response = result match {
      case MessageOk(msg, statusCode) => HttpResponse(status = statusCode, entity = HttpEntity(s"$msg\n"))
      case MessageFailed(msg, statusCode) => HttpResponse(status = statusCode, entity = HttpEntity(s"$msg\n"))
      case MessageGetBalanceReply(amount) =>
        HttpResponse(status = StatusCodes.OK, entity = HttpEntity(s"${amount.toString()}\n"))
      case MessageListReply(accounts) => HttpResponse(
        status = StatusCodes.OK,
        entity = HttpEntity(accounts.mkString("These accounts have been opened within the Bank:\n", "\n", "\n"))
      )
    }

    Marshalling.WithFixedContentType(ContentTypes.`text/plain(UTF-8)`, () => response)
  }

  def handleWrongAmount(amount: String) = complete(HttpResponse(
    status = StatusCodes.BadRequest, entity = HttpEntity(s"The requested amount is not parsable: $amount.")
  ))

  def toErrorResponse(x: Throwable) = complete(StatusCodes.InternalServerError, s"An error occurred: ${x.getMessage}")

  def getBalanceRoute(bank: Bank)(implicit timeout: Timeout): Route =
    path("balance" / LongNumber) { account =>
      get {
        onComplete(bank getBalance account) {
          case Success(x: ResultBankMessage) => complete(x)
          case Failure(NonFatal(x)) => toErrorResponse(x)
        }
      }
    }

  def getListRoute(bank: Bank)(implicit timeout: Timeout): Route =
    path("list") {
      get {
        onComplete(bank.listAccounts()) {
          case Success(x: ResultBankMessage) => complete(x)
          case Failure(NonFatal(x)) => toErrorResponse(x)
        }
      }
    }

  def getDepositeRoute(bank: Bank)(implicit timeout: Timeout): Route =
    path("deposit-through-cashier") {
      put {
        parameters("amount") { amount =>
          try {
            onComplete(bank debitViaCashier BigDecimal(amount)) {
              case Success(x: ResultBankMessage) => complete(x)
              case Failure(NonFatal(x)) => toErrorResponse(x)
            }
          } catch {
            case x: NumberFormatException => handleWrongAmount(amount)
          }
        }
      }
    }

  def getOpenRoute(bank: Bank)(implicit timeout: Timeout): Route =
    path("open-account" / LongNumber) { account =>
      post {
        onComplete(bank openAccount account) {
          case Success(x: ResultBankMessage) => complete(x)
          case Failure(NonFatal(x)) => toErrorResponse(x)
        }
      }
    }

  def getTransferRoute(bank: Bank)(implicit timeout: Timeout): Route =
    path("transfer" / LongNumber / LongNumber) { (from, to) =>
      put {
        parameters("amount") { amount =>
          try {
            onComplete(bank.transferMoney(from, to, BigDecimal(amount))) {
              case Success(x: ResultBankMessage) => complete(x)
              case Failure(NonFatal(x)) => toErrorResponse(x)
            }
          } catch {
            case x: NumberFormatException => handleWrongAmount(amount)
          }
        }
      }
    }

  def bankRouts(bank: Bank)(implicit timeout: Timeout): Route =
    getBalanceRoute(bank) ~ getListRoute(bank) ~ getDepositeRoute(bank) ~ getOpenRoute(bank) ~ getTransferRoute(bank)
}
