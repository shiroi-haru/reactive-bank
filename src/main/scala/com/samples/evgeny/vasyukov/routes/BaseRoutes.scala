package com.samples.evgeny.vasyukov.routes

import akka.http.scaladsl.server.Directives.get
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.PathDirectives.pathEndOrSingleSlash
import akka.http.scaladsl.server.directives.RouteDirectives.complete

object BaseRoutes {
  lazy val baseRoutes: Route =
    pathEndOrSingleSlash {
      get {
        complete("Welcome to the AkkBank!")
      }
    }
}
