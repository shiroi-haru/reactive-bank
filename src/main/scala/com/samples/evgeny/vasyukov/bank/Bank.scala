package com.samples.evgeny.vasyukov.bank

import akka.http.scaladsl.model.{ StatusCode, StatusCodes }

import scala.concurrent.Future

object BankMessages {

  sealed trait ResultBankMessage

  case class MessageOk(message: String = "Done.", status: StatusCode = StatusCodes.OK) extends ResultBankMessage

  case class MessageFailed(
    reasonMessage: String,
    status: StatusCode = StatusCodes.InternalServerError
  ) extends ResultBankMessage

  case class MessageGetBalanceReply(amount: BigDecimal) extends ResultBankMessage

  case class MessageListReply(accounts: Set[Long]) extends ResultBankMessage
}

trait Bank {
  import BankMessages._

  def openAccount(accountNo: Long): Future[ResultBankMessage]

  def getBalance(accountNo: Long): Future[ResultBankMessage]

  def transferMoney(from: Long, to: Long, amount: BigDecimal): Future[ResultBankMessage]

  def debitViaCashier(amount: BigDecimal): Future[ResultBankMessage]

  def listAccounts(): Future[ResultBankMessage]
}
