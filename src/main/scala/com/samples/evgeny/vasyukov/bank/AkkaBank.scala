package com.samples.evgeny.vasyukov.bank

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicLong

import akka.actor.{Actor, ActorNotFound, ActorRef, ActorSystem, InvalidActorNameException, Props}
import akka.event.LoggingReceive
import akka.http.scaladsl.model.{StatusCode, StatusCodes}
import akka.pattern.ask
import akka.util.Timeout
import com.samples.evgeny.vasyukov.bank.BankMessages._

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

/*

  Денег перевод,
  Словно журавль осенний -
  К "облаку" летит.

 */

private object BankInternalMessages {
  sealed trait BankMessage

  sealed trait InternalBankMessage extends BankMessage
  sealed trait ExternalBankMessage extends BankMessage

  case class MessageWithdraw(amount: BigDecimal) extends InternalBankMessage {
    require(amount > 0)
  }

  case class MessageDeposit(amount: BigDecimal) extends InternalBankMessage {
    require(amount > 0)
  }

  case class MessageTransfer(from: ActorRef, to: ActorRef, amount: BigDecimal) extends InternalBankMessage {
    require(amount > 0)
  }

  case class MessageCreateAccount(accountNo: Long) extends ExternalBankMessage

  case class MessageGetBalance(accountNo: Long) extends ExternalBankMessage

  case class DebitViaCashier(amount: BigDecimal) extends ExternalBankMessage

  case class MessageTransferMoney(from: Long, to: Long, amount: BigDecimal) extends ExternalBankMessage {
    require(amount > 0)
  }
}

class BankAccount extends Actor {
  import BankInternalMessages._

  var balance = BigDecimal(0.0)

  def receive = LoggingReceive {

    case MessageGetBalance(accountNo) =>
      sender ! MessageGetBalanceReply(balance)

    case MessageDeposit(amount) =>
      balance += amount
      sender ! MessageOk()

    case MessageWithdraw(amount) =>
      if (amount <= balance) {
        balance -= amount
        sender ! MessageOk()
      } else {
        sender ! MessageFailed("Not enough money on the source account.", StatusCodes.Forbidden)
      }

    case x => sender ! MessageFailed(s"Unexpected message: $x.")
  }
}

class Transferer extends Actor {
  import BankInternalMessages._

  def receive = LoggingReceive {
    case MessageTransfer(from, to, amount) =>
      from ! MessageWithdraw(amount)
      context.become(withdrawing(sender, to, amount))
  }

  def withdrawing(operator: ActorRef, to: ActorRef, amount: BigDecimal): Receive = LoggingReceive {
    case MessageOk(msg, status) =>
      to ! MessageDeposit(amount)
      context.become(depositing(operator))
    case x: MessageFailed =>
      operator ! x
      context.stop(self)
    case x =>
      sender ! MessageFailed(s"Unexpected message: $x.")
      context.stop(self)
  }

  def depositing(operator: ActorRef): Receive = LoggingReceive {
    case a @ MessageOk(msg, x) =>
      operator ! a
      context.stop(self)
    case x =>
      sender ! MessageFailed(s"Unexpected message: $x.")
      context.stop(self)
  }
}

class AkkaBank(system: ActorSystem, implicit val ec: ExecutionContext, implicit val timeout: Timeout) extends Bank {
  import BankInternalMessages._
  import com.samples.evgeny.vasyukov.FutureUtils._

  private val accounts = new ConcurrentHashMap[Long, Long]()
  private val transactionNo = new AtomicLong()
  private val cashier: ActorRef = createAccount(0L).get

  private def createAccount(accountNo: Long): Try[ActorRef] = Try {
    val r = system.actorOf(Props[BankAccount], accountActorName(accountNo))
    accounts.putIfAbsent(accountNo, accountNo)
    r
  }

  private def accountActorName(accountNo: Long): String = s"account:$accountNo"

  private def findAccountActor(name: String): RichFuture[ActorRef] = system.actorSelection(s"user/$name").resolveOne()

  private def notExists(number: Long, sc: StatusCode = StatusCodes.NotFound) =
    Future successful MessageFailed(s"Account no.$number doesn't exist.", sc)

  private def mustBePositive(amount: BigDecimal) =
    Future successful MessageFailed(s"Amount must be positive $amount", StatusCodes.Forbidden)

  override def openAccount(accountNo: Long): Future[ResultBankMessage] = {
    val name = accountActorName(accountNo)
    def ok() = {
      accounts.putIfAbsent(accountNo, accountNo)
      Future successful MessageOk(s"Account no.$accountNo was opened by the Bank.", status = StatusCodes.Created)
    }
    findAccountActor(name).flatMapAll {
      case Success(x) => Future successful MessageFailed(s"Account no.$accountNo already exists.", StatusCodes.Conflict)
      case Failure(e: ActorNotFound) => createAccount(accountNo) match {
        case Success(x) => ok()
        case Failure(e: InvalidActorNameException) => ok()
        case Failure(ex) => Future failed ex
      }
      case Failure(ex) => Future failed ex
    }
  }

  override def getBalance(accountNo: Long): Future[ResultBankMessage] =
    findAccountActor(accountActorName(accountNo)).flatMapAll {
      case Success(x) => (x ? MessageGetBalance(accountNo)).mapTo[MessageGetBalanceReply]
      case Failure(e: ActorNotFound) => notExists(accountNo)
      case Failure(ex) => Future failed ex
    }

  override def transferMoney(from: Long, to: Long, amount: BigDecimal): Future[ResultBankMessage] =
    if (amount <= 0) mustBePositive(amount)
    else findAccountActor(accountActorName(from)).flatMapAll {
      case Success(xfrom) =>
        findAccountActor(accountActorName(to)).flatMapAll {
          case Success(xto) =>
            val transaction = system.actorOf(Props[Transferer], s"${transactionNo.getAndIncrement()}tr${from}TO$to")
            (transaction ? MessageTransfer(xfrom, xto, amount)).mapTo[ResultBankMessage]
          case Failure(e: ActorNotFound) => notExists(to)
          case Failure(ex) => Future failed ex
        }
      case Failure(e: ActorNotFound) => notExists(from)
      case Failure(ex) => Future failed ex
    }

  override def debitViaCashier(amount: BigDecimal): Future[ResultBankMessage] =
    if (amount <= 0) mustBePositive(amount)
    else (cashier ? MessageDeposit(amount)).mapTo[ResultBankMessage]

  override def listAccounts(): Future[ResultBankMessage] =
    Future successful MessageListReply(accounts.keySet().asScala.toSet)
}
