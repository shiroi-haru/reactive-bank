package com.samples.evgeny.vasyukov

import com.samples.evgeny.vasyukov.bank.AkkaBank
import com.samples.evgeny.vasyukov.routes.{BankRoutes, BaseRoutes}

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives
import akka.stream.ActorMaterializer
import akka.util.Timeout

import scala.language.postfixOps
import scala.io.StdIn
import scala.util.{Failure, Success, Try}
import scala.concurrent.duration._

object ReactiveBankApp extends App with Directives with BankRoutes {

  override def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem("my-system")
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher
    implicit val timeout = Timeout(30 seconds)

    val routes = BaseRoutes.baseRoutes ~ bankRouts(new AkkaBank(system, executionContext, timeout))

    val port = args.headOption match {
      case Some(x) => Try { x.toShort } match {
        case Success(y) => y
        case Failure(e) =>
          System.err.println("Usage: sbt run <port>")
          System.exit(1); 0
      }
      case None => 8080
    }

    val bindingFuture = Http().bindAndHandle(routes, "localhost", port)

    println(s"The Akka Bank Server online at http://localhost:$port/\nPress RETURN to stop...")
    StdIn.readLine()
    bindingFuture.flatMap(_.unbind()).onComplete(_ => system.terminate())
  }
}
