package com.samples.evgeny.vasyukov

import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.Try

object FutureUtils {
  implicit class RichFuture[T](f: Future[T]) {
    def mapAll[U](pf: PartialFunction[Try[T], U])(implicit ec: ExecutionContext): Future[U] = {
      val p = Promise[U]()
      f onComplete (r => p complete Try(pf(r)))
      p.future
    }
    def flatMapAll[U](pf: PartialFunction[Try[T], Future[U]])(implicit ec: ExecutionContext): Future[U] = {
      val p = Promise[U]()
      f.onComplete { r => pf(r).onComplete { z => p complete z }(ec) }(ec)
      p.future
    }
  }
}
