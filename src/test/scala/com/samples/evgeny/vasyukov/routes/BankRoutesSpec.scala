package com.samples.evgeny.vasyukov.routes

import com.samples.evgeny.vasyukov.bank.AkkaBank

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.util.Timeout

import scala.language.postfixOps
import scala.util.{Success, Try}
import scala.concurrent.duration._

import org.scalatest.{Matchers, WordSpec}

class BankRoutesSpec extends WordSpec with Matchers with ScalatestRouteTest with BankRoutes {
  implicit val timeout = Timeout(30 seconds)
  val bank = new AkkaBank(system, system.dispatcher, timeout)

  def checkResultDecimal(): Try[BigDecimal] = Try { BigDecimal(checkResultString()) }

  def checkResultString() = responseAs[String].split('\n').toList.filter { _.trim.nonEmpty }.head.trim

  "SimpleRoute" should {
    "answer to GET requests to `/balance/0`, right after start" in {
      Get("/balance/0") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        checkResultDecimal() shouldBe Success(BigDecimal(0))
      }
    }
    "answer to GET requests to `/list`, right after start" in {
      Get("/list") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        val res = responseAs[String].split('\n').toList
        res.head shouldBe "These accounts have been opened within the Bank:"
        res.tail.toSet shouldBe Set("0")
      }
    }
    "answer to GET requests to `/balance/1`, right after start" in {
      Get("/balance/1") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.NotFound
        checkResultString() shouldBe "Account no.1 doesn't exist."
      }
    }
    "answer to POST requests to `/open-account/1`, right after start" in {
      Post("/open-account/1") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.Created
        checkResultString() shouldBe "Account no.1 was opened by the Bank."
      }
    }
    "answer to GET requests to `/list`, after creating 1" in {
      Get("/list") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        val res = responseAs[String].split('\n').toList
        res.head shouldBe "These accounts have been opened within the Bank:"
        res.tail.toSet shouldBe Set("0", "1")
      }
    }
    "answer to POST requests to `/open-account/2`, right after start" in {
      Post("/open-account/2") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.Created
        checkResultString() shouldBe "Account no.2 was opened by the Bank."
      }
    }
    "answer to GET requests to `/list`, after creating 0, 1 and 2" in {
      Get("/list") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        val res = responseAs[String].split('\n').toList
        res.head shouldBe "These accounts have been opened within the Bank:"
        res.tail.toSet shouldBe Set("0", "1", "2")
      }
    }
    "answer to POST requests to `/open-account/2`, after already creating it" in {
      Post("/open-account/2") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.Conflict
        checkResultString() shouldBe "Account no.2 already exists."
      }
    }
    "answer to PUT requests to `/deposit-through-cashier?amount=50.1`" in {
      Put("/deposit-through-cashier?amount=50.1") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        checkResultString() shouldBe "Done."
      }
    }
    "answer to GET requests to `/balance/0`, right after depositing some money via the cashier" in {
      Get("/balance/0") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        checkResultDecimal() shouldBe Success(BigDecimal(50.1))
      }
    }
    "answer to PUT requests to `/deposit-through-cashier?amount=49.9`" in {
      Put("/deposit-through-cashier?amount=49.9") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        checkResultString() shouldBe "Done."
      }
    }
    "answer to GET requests to `/balance/0`, right after depositing some more money via the cashier" in {
      Get("/balance/0") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        checkResultDecimal() shouldBe Success(BigDecimal(100))
      }
    }
    "answer to PUT requests to `/transfer/0/1?amount=9.99`" in {
      Put("/transfer/0/1?amount=9.99") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        checkResultString() shouldBe "Done."
      }
    }
    "answer to GET requests to `/balance/0`, after moving some money" in {
      Get("/balance/0") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        checkResultDecimal() shouldBe Success(BigDecimal(90.01))
      }
    }
    "answer to GET requests to `/balance/1`, after depositing some money to" in {
      Get("/balance/1") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        checkResultDecimal() shouldBe Success(BigDecimal(9.99))
      }
    }
    "answer to PUT requests to `/transfer/0/1?amount=10.01`" in {
      Put("/transfer/0/1?amount=10.01") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        checkResultString() shouldBe "Done."
      }
    }
    "answer to GET requests to `/balance/0`, after moving some more money away" in {
      Get("/balance/0") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        checkResultDecimal() shouldBe Success(BigDecimal(80))
      }
    }
    "answer to GET requests to `/balance/1`, after depositing some more money to" in {
      Get("/balance/1") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        checkResultDecimal() shouldBe Success(BigDecimal(20))
      }
    }
    "answer to PUT requests to `/transfer/1/2?amount=10.00`" in {
      Put("/transfer/1/2?amount=10.00") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        checkResultString() shouldBe "Done."
      }
    }
    "answer to GET requests to `/balance/1`, after moving some money away to 2" in {
      Get("/balance/1") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        checkResultDecimal() shouldBe Success(BigDecimal(10))
      }
    }
    "answer to GET requests to `/balance/2`, after depositing some money from 1" in {
      Get("/balance/2") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        checkResultDecimal() shouldBe Success(BigDecimal(10))
      }
    }
    "answer to PUT requests to `/transfer/1/2?amount=10.01`" in {
      Put("/transfer/1/2?amount=10.01") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.Forbidden
        checkResultString() shouldBe "Not enough money on the source account."
      }
    }
    "answer to PUT requests to `/deposit-through-cashier?amount=-0.1`" in {
      Put("/deposit-through-cashier?amount=-0.1") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.Forbidden
        checkResultString() shouldBe "Amount must be positive -0.1"
      }
    }
    "answer to GET requests to `/balance/0`, after failed attempt to deposit negative amount" in {
      Get("/balance/0") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        checkResultDecimal() shouldBe Success(BigDecimal(80))
      }
    }
    "answer to PUT requests to `/transfer/1/2?amount=-0.1`" in {
      Put("/transfer/1/2?amount=-0.1") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.Forbidden
        checkResultString() shouldBe "Amount must be positive -0.1"
      }
    }
    "answer to GET requests to `/balance/1`, after failed attempt to deposit negative amount" in {
      Get("/balance/1") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        checkResultDecimal() shouldBe Success(BigDecimal(10))
      }
    }
    "answer to GET requests to `/list`, after creating 1 and 2" in {
      Get("/list") ~> bankRouts(bank) ~> check {
        status shouldBe StatusCodes.OK
        val res = responseAs[String].split('\n').toList
        res.head shouldBe "These accounts have been opened within the Bank:"
        res.tail.toSet shouldBe Set("0", "1", "2")
      }
    }
    "not handle a POST request to `/hello`" in {
      Post("/hello") ~> bankRouts(bank) ~> check {
        handled shouldBe false
      }
    }
  }
}
