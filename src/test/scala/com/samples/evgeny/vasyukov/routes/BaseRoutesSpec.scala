package com.samples.evgeny.vasyukov.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.{ Matchers, WordSpec }

class BaseRoutesSpec extends WordSpec with Matchers with ScalatestRouteTest {
  "BaseRoute" should {
    "answer to get request to `/`" in {
      Get("/") ~> BaseRoutes.baseRoutes ~> check {
        status shouldBe StatusCodes.OK
        responseAs[String] shouldBe "Welcome to the AkkBank!"
      }
    }
  }
}
