1. Basic idea
~~~~~~~~~~~~~~~~

  This example application is the simplest model of a banking accounting
system. It is written in Scala and utilizes Akka actor system in order
to demonstrate reactive approach to implementing RESTful services
("Server").

  The idea is to implement a standalone reactive server, which is
responsible for receiving and evaluating requests from another component
of a complex system ("Client", which is not presented here). Server
handles a set of "accounts", which are represented as:
1) basically a Long number of an Account Number and
2) a BigDecimal number, containing the current Account's Balance.

  After the Server starts, it has already a special "Cashier" Account
with the Account Number 0 with zero balance. Money can come into the
accounting system, represented by the Server, only through deposits to
this account via special API method, which is described below.

  In order to cause any transformation of account's data, handled by the
Server, the Client should specify an Account Number as an argument to
the API action, except for depositing money via the special "Cashier"
Account Number 0, mentioned earlier.


 2. The API:
~~~~~~~~~~~~~~

0) All Client’s requests should not be sent with any bodies within
requests, for the sake of simplicity for testing from Linux cmd-line.
All Server responses from the Server have "TEXT/PLAIN; CHARSET=UTF8"
mime-type.

1) Listing all Account Numbers, which were opened within the Server:

   GET http://127.0.0.1:8080/list

   which will respond with a set of Account Numbers, separated by the 
"\n" symbol after the message: 
"These accounts have been opened within the Bank:\n"


2) Checking the current Account Balance, where {AccountNumber} should be
replaced by the actual Account Number, which is known to the Client (as
mentioned in section 1):

   GET http://127.0.0.1:8080/balance/{AccountNumber}

which will respond with a textual representation of a current Account's
Balance if the specified account already exists in the Server. Otherwise
the message is emitted as an answer to the Client: 
"Account no. {AccountNumber} doesn't exist."

3) Opening (Creating) new Account, where {AccountNumber} should be 
replaced by the actual Account Number, which should be known to the
Client (as mentioned in section 1):

   POST http://127.0.0.1:8080/open-account/{AccountNumber}

which will respond with a string "Account no. {AccountNumber} was opened
by the Bank." if the specified account doesn't exist in the Server.
Otherwise the message is emitted as an answer to the Client:
"Account no. {AccountNumber} already exists."

4) Depositing some money of specified {Amount} (which must be a positive
floating-point decimal number) to the special "Cashier" Account (Number
0):

   PUT http://127.0.0.1:8080/deposit-through-cashier?amount={Amount}

which will respond with a string "Done.". The current balance of the
Account Number 0 will be increased by the specified {Amount}. In case if
the specified amount is not positive, the string "Amount must be
positive {Amount}" will be emitted.

5) Transferring some money of specified {Amount} between Accounts within
the Server, from {AccountFrom} to {AccountTo}:

   PUT http://127.0.0.1:8080/transfer/{AccountFrom}/{AccountTo}?amount={Amount}

which will respond with a string "Done.". The current balance of the
Account Number {AccountFrom} will be decreased by the specified {Amount}
and the current balance of the Account Number {AccountTo} will be
increased by the same amount.
In case if the specified amount is not positive, the string "Amount must
be positive {Amount}" will be emitted.
In case if there is less money on the Account Number {AccountFrom} than
{Amount}, the string:
"Not enough money on the source account." will be emitted as an answer
to the Client.


 3. Installing:
~~~~~~~~~~~~~~~~~

The server application could be built and run with the help of the
standard Scala Build Tool - SBT utility.
Default SBT goals are meaningful in this case:

"sbt compile" to build the application;
"sbt test" to run the whole test set;
"sbt run 8080" to run the Server application, which will listen on 8080
system port for Client's RESTful requests.